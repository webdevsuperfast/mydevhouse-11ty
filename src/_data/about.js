module.exports = [
  {
    title: 'Our Team',
    slug: 'our-team',
    url: '#our-team',
  },
  {
    title: 'Our Process',
    slug: 'our-process',
    url: '#our-process',
  },
  {
    title: 'Our Services',
    slug: 'our-services',
    url: '#our-services',
  },
]
